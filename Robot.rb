class Robot
  def initialize(position)
    @position = position;
  end

  def move()
    x=@position.instance_variable_get(:@x)
    y=@position.instance_variable_get(:@y)
    case @position.getFacing
      when "east"
        y = @position.instance_variable_get(:@y) + 1;
      when "west"
        y = @position.instance_variable_get(:@y) - 1;
      when "north"
        x = @position.instance_variable_get(:@x) + 1;
      when "south"
        x = @position.instance_variable_get(:@x) - 1;
    end
    if x > 4 || x < 0 || y > 4 || y < 0
      return false
    else
      @position.instance_variable_set(:@x, x)
      @position.instance_variable_set(:@y, y)
      return true;
    end
  end

  def left
    turn ("left")
  end

  def right
    turn "right"
  end

  def getPointer
    pointer = ""
    case @position.instance_variable_get(:@facing)
      when "north"
        pointer = "^"
      when "south"
        pointer = "v"
      when "east"
        pointer = ">"
      when "west"
        pointer = "<"
    end
    return pointer
  end

  def printMatrix (n)
    pointer = getPointer
    puts "(beta!)"
    for i in 0..n
      for j in 0..n
        if !(@position.instance_variable_get(:@x) == n-i && @position.instance_variable_get(:@y) == j)
          if i == 0 && j !=n
            print " _"
          elsif j == n && i != 0
            print "|"
          elsif j !=n && i!=0
            print "|_"
          end
        else
          print "|" + getPointer
        end
      end
      puts
    end
  end

  def turn (direction)
    case direction
      when "right"
        case @position.instance_variable_get(:@facing)
          when "north"
            @position.instance_variable_set(:@facing, "east")
          when "east"
            @position.instance_variable_set(:@facing, "south")
          when "south"
            @position.instance_variable_set(:@facing, "west")
          when "west"
            @position.instance_variable_set(:@facing, "north")
        end
      when "left"
        case @position.instance_variable_get(:@facing)
          when "north"
            @position.instance_variable_set(:@facing, "west")
          when "east"
            @position.instance_variable_set(:@facing, "north")
          when "south"
            @position.instance_variable_set(:@facing, "east")
          when "west"
            @position.instance_variable_set(:@facing, "south")
        end
    end
  end

  def report
    printMatrix (5)
    puts @position.instance_variable_get(:@x).to_s + "," + @position.instance_variable_get(:@y).to_s + "," + @position.instance_variable_get(:@facing).upcase + "\n"
  end
end