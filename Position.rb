class Position
  def initialize(x, y, facing)
    @x = x;
    @y = y;
    @facing = facing;
  end

  def getFacing
    return @facing
  end
end