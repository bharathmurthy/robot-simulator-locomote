require __dir__ + '/Robot.rb'
require __dir__ + '/Position.rb'

module Commands
  PLACE = "place"
  MOVE = "move"
  LEFT = "left"
  RIGHT = "right"
  REPORT = "report"
  QUIT = "quit"
end
l = 1;
robot = nil;


def menu
  puts "#########################################################"
  puts "Type any of below commands to start playing with Robot!!\n"
  puts "PLACE X,Y,FACING\n"
  puts "MOVE\n"
  puts "LEFT\n"
  puts "RIGHT\n"
  puts "REPORT\n"
  puts "QUIT (if you have troubled your robot enough :p)\n"
  puts "##########################################################\n"
  puts "Enter your next command -> "
end

def checkFacing facing
  directions = ["north", "south", "east", "west"]
  if directions.include? facing
    return true
  else
    return false
  end
end

def clearScreen
  if RUBY_PLATFORM =~ /win32|win64|\.NET|windows|cygwin|mingw32/i
    system('cls')
  else
    system('clear')
  end
end

while l
  menu
  placeRobot = gets.downcase.chomp
  placeRobot = placeRobot.split
  case placeRobot[0]
    when Commands::PLACE
      if (placeRobot[1] == nil)
        puts "cannot place the Robot in vaccum X-["
      else
      position = placeRobot[1].split(',')
      x = Integer position[0] rescue false
      y = Integer position[1] rescue false
      if !(x && y)
        puts "Check if you have x or y co-ordinates as Integer(Number)"
      else
        x = position[0].to_i
        y = position[1].to_i
        if ((x <= 4 && x >= 0) && (y <= 4 && y >= 0))
          if checkFacing(position[2])
            pos = Position.new(x, y, position[2])
            robot = Robot.new(pos);
          else
            puts "you sure you know the proper directions?? where did you study? don't kill my Robot X-[ \n"
          end
        else
          puts "Dangerous placement!, Robot was about to be destroyed but don't worry I have your back :).\n"
        end
      end
      end

    when Commands::MOVE
      if (robot == nil)
        puts "Oh ho! you don't have any robot yet to move around"
      else
        if (!robot.move)
          puts "Dangerous move!, Robot was about to be destroyed but don't worry I have your back :).\n"
        end
      end
    when Commands::LEFT
      if (robot == nil)
        puts "Oh ho! you don't have any robot yet to turn around"
      else
        robot.left
      end
    when Commands::RIGHT
      if (robot == nil)
        puts "Oh ho! you don't have any robot yet to turn around"
      else
        robot.right
      end
    when Commands::REPORT
      if (robot == nil)
        puts "Oh ho! you don't have any robot yet to report it's status"
      else
        robot.report
      end
    when Commands::QUIT
      break
    else
      puts "Not a valid command buddy, Try again using the menu Provided\n"
  end
  puts "Press enter to continue"
  gets
  clearScreen
end
puts "Good Bye!"